$(document).ready(function() {
  $('.community #core-team').addClass('animated zoomIn');
  $('.community #contributors').addClass('animated zoomIn');
  $('.community #documentation').addClass('animated zoomIn');
  $('.community #help').addClass('animated zoomIn');
  $('.community #contributing').addClass('animated zoomIn');
  $('.community #applications').addClass('animated zoomIn');
  $('.community #mvp').addClass('animated zoomIn');
});
